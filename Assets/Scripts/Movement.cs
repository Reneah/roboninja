﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField]
    int speed = 8;

    Rigidbody2D player;

    float movementY;
    float movementX;

    private void Start()
    {
        player = GetComponent<Rigidbody2D>();
    }




    void Update()
    {
        float movementY = Input.GetAxis("Vertical");
        float movementX = Input.GetAxis("Horizontal");

        player.velocity = new Vector2(movementX * speed, movementY * speed);

       


    }
}
